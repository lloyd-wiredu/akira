# --- Sample dataset

# --- !Ups

insert into account (ID, User_ID, Password, Role) values (1,'patient1@gmail.com','password','patient');
insert into account (ID, User_ID, Password, Role) values (2,'patient2@gmail.com','password','patient');
insert into account (ID, User_ID, Password, Role) values (3,'patient3@gmail.com','password','patient');
insert into account (ID, User_ID, Password, Role) values (4,'patient4@gmail.com','password','patient');
insert into account (ID, User_ID, Password, Role) values (5,'patient5@gmail.com','password','patient');
insert into account (ID, User_ID, Password, Role) values (6,'patient6@gmail.com','password','patient');
insert into account (ID, User_ID, Password, Role) values (7,'patient7@gmail.com','password','patient');
insert into account (ID, User_ID, Password, Role) values (8,'patient8@gmail.com','password','patient');
insert into account (ID, User_ID, Password, Role) values (11,'doctor1@akira.md','password','doctor');
insert into account (ID, User_ID, Password, Role) values (12,'doctor2@akira.md','password','doctor');
insert into account (ID, User_ID, Password, Role) values (13,'doctor3@akira.md','password','doctor');
insert into account (ID, User_ID, Password, Role) values (14,'doctor4@akira.md','password','doctor');
insert into account (ID, User_ID, Password, Role) values (21,'admin@akira.md','password','admin');

insert into patient (ID, First_NM, Last_NM, Account_ID) values (1,'John','One',1);
insert into patient (ID, First_NM, Last_NM, Account_ID) values (2,'Tracy','Two',2);
insert into patient (ID, First_NM, Last_NM, Account_ID) values (3,'Mike','Three',3);
insert into patient (ID, First_NM, Last_NM, Account_ID) values (4,'Sarah','Four',4);
insert into patient (ID, First_NM, Last_NM, Account_ID) values (5,'Frank','Five',5);
insert into patient (ID, First_NM, Last_NM, Account_ID) values (6,'Jessica','Six',6);
insert into patient (ID, First_NM, Last_NM, Account_ID) values (7,'Steven','Seven',7);
insert into patient (ID, First_NM, Last_NM, Account_ID) values (8,'Emily','Eight',8);

insert into doctor (ID, First_NM, Last_NM, Account_ID, License_STR) values (11,'Bruce','Banner',11,'123456');
insert into doctor (ID, First_NM, Last_NM, Account_ID, License_STR) values (12,'Clark','Kent',12,'234561');
insert into doctor (ID, First_NM, Last_NM, Account_ID, License_STR) values (13,'Selina','Kyle',13,'345612');
insert into doctor (ID, First_NM, Last_NM, Account_ID, License_STR) values (14,'Diana','Prince',14,'456123');

insert into admin (ID, First_NM, Last_NM, Account_ID) values (21,'Akira','Admin',21);

# --- !Downs

delete from account
delete from patient
delete from admin
delete from doctor