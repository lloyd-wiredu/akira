# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table account (
  id                        bigint(20) not null,
  User_ID                   varchar(255) not null,
  Password                  varchar(255) not null,
  Role                      varchar(255) not null,
  created                   DATETIME DEFAULT CURRENT_TIMESTAMP not null,
  updated                   DATETIME DEFAULT CURRENT_TIMESTAMP not null,
  constraint uq_account_User_ID unique (User_ID),
  constraint pk_account primary key (id))
;

create table admin (
  id                        bigint(20) not null,
  Account_ID                bigint(20),
  First_NM                  varchar(50) not null,
  Last_NM                   varchar(50) not null,
  created                   DATETIME DEFAULT CURRENT_TIMESTAMP not null,
  updated                   DATETIME DEFAULT CURRENT_TIMESTAMP not null,
  constraint pk_admin primary key (id))
;

create table appointment (
  id                        bigint(20) not null,
  Patient_ID                bigint(20) not null,
  Doctor_ID                 bigint(20),
  StartTime_DT              timestamp not null,
  EndTime_DT                timestamp not null,
  created                   DATETIME DEFAULT CURRENT_TIMESTAMP not null,
  updated                   DATETIME DEFAULT CURRENT_TIMESTAMP not null,
  constraint pk_appointment primary key (id))
;

create table doctor (
  id                        bigint(20) not null,
  Account_ID                bigint(20),
  First_NM                  varchar(50) not null,
  Last_NM                   varchar(50) not null,
  License_STR               varchar(50) not null,
  created                   DATETIME DEFAULT CURRENT_TIMESTAMP not null,
  updated                   DATETIME DEFAULT CURRENT_TIMESTAMP not null,
  constraint pk_doctor primary key (id))
;

create table patient (
  id                        bigint(20) not null,
  Account_ID                bigint(20),
  First_NM                  varchar(50) not null,
  Last_NM                   varchar(50) not null,
  created                   DATETIME DEFAULT CURRENT_TIMESTAMP not null,
  updated                   DATETIME DEFAULT CURRENT_TIMESTAMP not null,
  constraint pk_patient primary key (id))
;

create sequence account_seq;

create sequence admin_seq;

create sequence appointment_seq;

create sequence doctor_seq;

create sequence patient_seq;

alter table admin add constraint fk_admin_account_1 foreign key (Account_ID) references account (ID) on delete restrict on update restrict;
create index ix_admin_account_1 on admin (Account_ID);
alter table appointment add constraint fk_appointment_patient_2 foreign key (Patient_ID) references patient (ID) on delete restrict on update restrict;
create index ix_appointment_patient_2 on appointment (Patient_ID);
alter table appointment add constraint fk_appointment_doctor_3 foreign key (Doctor_ID) references doctor (ID) on delete restrict on update restrict;
create index ix_appointment_doctor_3 on appointment (Doctor_ID);
alter table doctor add constraint fk_doctor_account_4 foreign key (Account_ID) references account (ID) on delete restrict on update restrict;
create index ix_doctor_account_4 on doctor (Account_ID);
alter table patient add constraint fk_patient_account_5 foreign key (Account_ID) references account (ID) on delete restrict on update restrict;
create index ix_patient_account_5 on patient (Account_ID);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists account;

drop table if exists admin;

drop table if exists appointment;

drop table if exists doctor;

drop table if exists patient;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists account_seq;

drop sequence if exists admin_seq;

drop sequence if exists appointment_seq;

drop sequence if exists doctor_seq;

drop sequence if exists patient_seq;

