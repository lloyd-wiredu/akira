package controllers.doctor.appointments;

import java.util.List;

import interfaces.RoleAuth;
import models.appointment.Appointment;
import models.appointment.AppointmentFinder;
import models.appointment.AppointmentModifier;
import models.appointment.exceptions.AppointmentNotFoundException;
import models.appointment.exceptions.ModifiedException;
import models.role.provider.Doctor;
import models.role.provider.DoctorFinder;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security.Authenticated;

@Authenticated(RoleAuth.class)
public class DoctorAppointmentControl extends Controller {

	public Result viewAppointments(){
		String userId = session().get("userId");
		Doctor doctor = DoctorFinder.findDoctorByUserId(userId);
		if(doctor != null){
			List<Appointment> appts = AppointmentFinder.findValidUnconfirmedForDoctor(doctor);
			return ok(views.html.doctor.appointment.view.render(appts));
		}
		return badRequest();
	}
	
	public Result attachDoctorToAppointment(Long appointmentId){
		String userId = session().get("userId");
		Doctor doctor = DoctorFinder.findDoctorByUserId(userId);
		try {
			AppointmentModifier.attachDoctorToAppointment(appointmentId, doctor);
		} catch (AppointmentNotFoundException | ModifiedException e) {
			Logger.error(e.getMessage());
		}
		return redirect(controllers.doctor.appointments.routes.DoctorAppointmentControl.viewAppointments());
	}
	
	
}
