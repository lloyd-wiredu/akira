package controllers.patient.appointments;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import interfaces.RoleAuth;
import models.appointment.Appointment;
import models.appointment.AppointmentFactory;
import models.appointment.AppointmentFinder;
import models.appointment.AppointmentModifier;
import models.appointment.exceptions.AppointmentNotFoundException;
import models.appointment.exceptions.ConflictingTimeException;
import models.appointment.exceptions.ModifiedException;
import models.appointment.exceptions.TimeOutOfBoundsException;
import models.role.patient.Patient;
import models.role.patient.PatientFinder;
import play.Logger;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security.Authenticated;

@Authenticated(RoleAuth.class)
public class PatientAppointmentControl extends Controller {

	public Result viewAppointments(){
		String userId = session().get("userId");
		Patient patient = PatientFinder.findPatientByUserId(userId);
		List<Appointment> appts = AppointmentFinder.findAllByPatient(patient);
		return ok(views.html.patient.appointment.view.render(appts, new DynamicForm()));
	}

	public Result newAppointment(){
		// Bind and process form fields.
		DynamicForm timeForm = DynamicForm.form().bindFromRequest();
		Logger.debug(timeForm.toString());
		long time, max;
		int days, duration;
		String userId = session().get("userId");
		Patient patient = PatientFinder.findPatientByUserId(userId);
		
		try {
			time = Long.parseLong(timeForm.get("time"));
			max = Long.parseLong(timeForm.get("maxTime"));
			days = Integer.parseInt(timeForm.get("days"));
			duration = Integer.parseInt(timeForm.get("duration"));
			
			// Create new appointment entry. Throw exception is it fails.
			Appointment appt = AppointmentFactory.newAppointment(time, max, days, duration, patient);
		} catch (NumberFormatException e){
			timeForm.reject("Your request is incorrect. Please try again.");
		} catch (ConflictingTimeException | TimeOutOfBoundsException e){
			// Add error message if exception is thrown for out of bounds error or time conflict.
			timeForm.reject(e.getMessage());
		}
		
		List<Appointment> appts = AppointmentFinder.findAllByPatient(patient);
		if(timeForm.hasGlobalErrors() || timeForm.hasErrors()){
			// return badrequest with submitted form with rejection error message.
			return badRequest(views.html.patient.appointment.view.render(appts, timeForm));
		} else {
			// return return with brand new form
			return ok(views.html.patient.appointment.view.render(appts, new DynamicForm()));
		}
	}
	
	public Result cancelAppointment(Long appointmentId){
		try {
			AppointmentModifier.cancelAppointment(appointmentId);
		} catch (AppointmentNotFoundException | ModifiedException e) {
			Logger.error(e.getMessage());
		}
		return redirect(controllers.patient.appointments.routes.PatientAppointmentControl.viewAppointments());
	}
	
	
}
