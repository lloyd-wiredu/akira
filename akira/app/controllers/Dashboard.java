package controllers;

import play.mvc.Controller;
import play.mvc.Result;

public class Dashboard extends Controller {

	public Result main(String role){
		if(role.equals("admin")){
			return redirect(controllers.admin.appointments.routes.AdminAppointmentControl.viewAppointments());
		} else if(role.equals("doctor")){
			return redirect(controllers.doctor.appointments.routes.DoctorAppointmentControl.viewAppointments());			
		} else if(role.equals("patient")){
			return redirect(controllers.patient.appointments.routes.PatientAppointmentControl.viewAppointments());			
		}
		return unauthorized("Access denied.");
	}
	
}
