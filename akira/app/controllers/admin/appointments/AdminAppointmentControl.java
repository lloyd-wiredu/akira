package controllers.admin.appointments;

import java.util.List;

import interfaces.RoleAuth;
import models.appointment.Appointment;
import models.appointment.AppointmentFinder;
import models.appointment.AppointmentModifier;
import models.appointment.exceptions.AppointmentNotFoundException;
import models.appointment.exceptions.ModifiedException;
import models.role.provider.Doctor;
import models.role.provider.DoctorFinder;
import play.Logger;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security.Authenticated;

@Authenticated(RoleAuth.class)
public class AdminAppointmentControl extends Controller {

	public Result viewAppointments(){
		List<Appointment> appts = AppointmentFinder.findAll();
		return ok(views.html.admin.appointment.view.render(appts, new DynamicForm()));
	}
	
	public Result removeDoctorFromAppointment(Long appointmentId){
		try {
			AppointmentModifier.removeDoctorFromAppointment(appointmentId);
		} catch (AppointmentNotFoundException | ModifiedException e) {
			Logger.error(e.getMessage());
		}
		return redirect(controllers.admin.appointments.routes.AdminAppointmentControl.viewAppointments());
	}

	public Result attachDoctorToAppointment(Long appointmentId){
		// Bind dropdown submission from request and process
		DynamicForm docForm = DynamicForm.form().bindFromRequest();
		long doctorId;
		Doctor doctor = null;
		List<Appointment> appts = null;
		try {
			doctorId = Long.parseLong(docForm.get("doctorId"));
			doctor = DoctorFinder.findDoctorById(doctorId);
			
			// Modify existing appointment or exception is thrown
			AppointmentModifier.attachDoctorToAppointment(appointmentId, doctor);
			// Get new set of up-to-date appointments
			appts = AppointmentFinder.findAll();
		} catch (NumberFormatException e) {
			docForm.reject("Invalid selection");
		} catch (AppointmentNotFoundException | ModifiedException e) {
			// return badrequest with submitted form with rejection error message.
			docForm.reject(e.getMessage());
		}
		return ok(views.html.admin.appointment.view.render(appts, docForm));
	}
	
}
