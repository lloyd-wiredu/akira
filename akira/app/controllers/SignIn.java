package controllers;

import play.mvc.*;
import play.data.*;
import static play.data.Form.*;

import views.html.signin.*;

import models.*;
import models.account.Account;
import models.account.AccountFinder;
import models.account.AccountValidator;
import models.account.UserForm;

public class SignIn extends Controller {
    
    /**
     * Defines a form wrapping the UserForm class.
     */ 
    final static Form<UserForm> signinForm = form(UserForm.class);
  
    /**
     * Display a blank form.
     */ 
    public Result blank() {
    	session().clear();
        return ok(form.render(signinForm));
    }
    
    /**
     * Handle the form submission.
     */
    public Result submit() {
        Form<UserForm> filledForm = signinForm.bindFromRequest();
        Account verifAccount = null;
        // Check if the username/password is valid
        if(!filledForm.hasErrors()) {
        	String userId = filledForm.get().email;
        	String password = filledForm.get().password;
        	verifAccount = AccountFinder.findValidAccount(userId, password);
        	if(verifAccount == null){
        		filledForm.reject("Invalid Credentials");
        	}
        }
        if(filledForm.hasErrors() || filledForm.hasGlobalErrors()) {
            return badRequest(form.render(filledForm));
        } else {
        	session().put("userId", verifAccount.getUserId());
        	session().put("role", verifAccount.getRole());
        	return redirect(controllers.routes.Dashboard.main(verifAccount.getRole()));
        }
    }
  
    /**
     * Handle sign out.
     */
    public Result signOut(){
    	session().clear();
    	return redirect(controllers.routes.SignIn.blank());
    }
    
    
}