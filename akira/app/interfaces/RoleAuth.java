package interfaces;

import models.account.AccountFinder;
import play.mvc.*;
import play.mvc.Http.*;
public class RoleAuth extends Security.Authenticator {

    @Override
    public String getUsername(Context ctx) {
    	if(!ctx.session().containsKey("userId") || !ctx.session().containsKey("role")) return null;
    	else if(!ctx.request().uri().contains(ctx.session().get("role"))) return null;
    	else return ctx.session().get("userId");
    }

    @Override
    public Result onUnauthorized(Context ctx) {
    	return unauthorized("Access Denied.");
    }
    
}
