package models.account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.avaje.ebean.Model.Finder;

import models.baseModel.BaseModel;

@Entity
@Table
public class Account extends BaseModel {

	@Column(name="User_ID", unique=true, nullable=false)
	private String userId;
	
	@Column(name="Password", nullable=false)
	private String password;

	@Column(name="Role", nullable=false)
	private String role;
	
	public Account(String userId, String password, String role){
		this.userId = userId;
		this.password = password;
		this.role = role;
	}

	// Getters & Setters
	public String getUserId(){
		return userId;
	}
	public String getPassword(){
		return password;
	}
	public String getRole(){
		return role;
	}
	
	public void setUserId(String userId){
		this.userId = userId;
	}
	public void setPassword(String password){
		this.password = password;
	}
	public void setRole(String role){
		this.role = role;
	}
	
	public static Finder<Long, Account> find = new Finder<>(Account.class);
}
