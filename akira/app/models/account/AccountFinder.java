package models.account;

public class AccountFinder {

	public static Account findByUserId(String userId){
		Account result = Account.find.where().eq("User_ID", userId).findUnique();
		return result;
	}

	/**
	 * Finds a valid account with given credentials 
	 * @param userId 
	 * @param password
	 * @return An account object if user credentials are valid, or null if not valid.
	 */
	public static Account findValidAccount(String userId, String password){
		Account verifAccount = AccountFinder.findByUserId(userId);
		if(verifAccount != null){
			// An encrypt module (like BCrypt) would be used here if the password was encrypted 
			if(verifAccount.getPassword().equals(password)){
				return verifAccount;
			}
		}
		return null;
	}
}
