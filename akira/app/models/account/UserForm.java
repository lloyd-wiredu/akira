package models.account;

import javax.validation.*;

import play.data.validation.Constraints.*;

public class UserForm {
    public String email;
    public String password;
    
    public UserForm() {}
    
    public UserForm(String email, String password) {
        this.email = email;
        this.password = password;
    }
    
}