package models.account;

public class AccountValidator {
	public static boolean isValidCredentials(String userId, String password){
		Account verifAccount = AccountFinder.findValidAccount(userId, password);
		return verifAccount != null;
	}
}
