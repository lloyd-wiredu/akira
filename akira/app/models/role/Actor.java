package models.role;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import models.account.Account;
import models.baseModel.BaseModel;

@MappedSuperclass
public class Actor extends BaseModel {

	   	private static final long serialVersionUID = 1L;
	    
	   	// An actors associated user account
	    @JoinColumn(name="Account_ID", referencedColumnName="ID")
		@ManyToOne
	    private Account account;
	   	
		@Column(name="First_NM", length=50, nullable=false)
		private String firstName;
		
		@Column(name="Last_NM", length=50, nullable=false)
		private String lastName;
	
		// Other relevant information about actor may go here .. 
		
		public Actor(){
			super();
		}
		

		//Getters and Setters.
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		
		public String getFullName(){
			return this.firstName+" "+this.lastName;
		}

}
