package models.role.provider;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import models.role.Actor;

@Entity
@Table
public class Doctor extends Actor {

	// License ID (eg. CPSO)
	@Column(name="License_STR", length=50, nullable=false)
	private String license;
	
	public String getLicense(){
		return license;
	}
	void setLicense(String license){
		this.license = license;
	}

	public String getFullName(){
		return "Dr. " + super.getFullName();		
	}
	
	public static Finder<Long, Doctor> find = new Finder<>(Doctor.class);
}
