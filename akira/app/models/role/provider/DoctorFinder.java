package models.role.provider;

import java.util.ArrayList;
import java.util.List;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Query;

import models.appointment.Appointment;
import play.Logger;

public class DoctorFinder {

	/**
	 * Find doctor by account user id
	 * @param userId
	 * @return a single doctor.
	 */
	public static Doctor findDoctorByUserId(String userId){
		Doctor doctor = Doctor.find.where()
								   .eq("account.userId", userId)
								   .findUnique();
		return doctor;
	}

	/**
	 * Find doctor by a given ID.
	 * @param doctorId
	 * @return a single doctor.
	 */
	public static Doctor findDoctorById(Long doctorId) {
		Doctor doctor = Doctor.find.byId(doctorId);
		return doctor;
	}
	
	/**
	 * Find all doctors who are available for a given appointment
	 * @param appointment
	 * @return all available doctors
	 */
	public static List<Doctor> findAvailableDoctorsForAppointment(Appointment appointment){
		List<Doctor> result = new ArrayList<Doctor>();
		// Nested Query
		// Step 1. Find all doctors with confirmed appointments that conflict with specified appointment. 
		Query<Appointment> subQuery =  Appointment.find.select("doctor.id")
												  .where()
												  .ne("Doctor_ID", null)
												  .disjunction()
													 .and(Expr.le("StartTime_DT", appointment.getStartTime()), Expr.gt("EndTime_DT", appointment.getStartTime()))
													 .and(Expr.lt("StartTime_DT", appointment.getEndTime()), Expr.ge("EndTime_DT", appointment.getEndTime()))
												  .endJunction()
												  .query(); 
		
		// Step 2. Find all doctors who are not in the list of doctors with conflicts.
		result = Doctor.find.select("id as Doctor_ID").where()
							.not(Expr.in("id", subQuery))  
							.findList(); 
		return result;	
	}
}
