package models.role.patient;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.avaje.ebean.Model.Finder;

import models.role.Actor;

@Entity
@Table
public class Patient extends Actor {

	public static Finder<Long, Patient> find = new Finder<>(Patient.class);
	
}
