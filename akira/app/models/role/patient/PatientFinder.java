package models.role.patient;

public class PatientFinder {

	/**
	 * Find patient by account user id
	 * @param userId
	 * @return a single patient
	 */
	public static Patient findPatientByUserId(String userId){
		Patient patient = Patient.find.where()
								   .eq("account.userId", userId)
								   .findUnique();
		return patient;
	}
	
}
