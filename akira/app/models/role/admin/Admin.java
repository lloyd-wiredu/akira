package models.role.admin;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.avaje.ebean.Model.Finder;

import models.role.Actor;

@Entity
@Table
public class Admin extends Actor {

	public static Finder<Long, Admin> find = new Finder<>(Admin.class);
	
}
