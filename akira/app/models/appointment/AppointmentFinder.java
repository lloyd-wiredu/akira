package models.appointment;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Query;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;

import models.role.patient.Patient;
import models.role.provider.Doctor;
import play.Logger;

public class AppointmentFinder {

	/**
	 * Find specified appointment.
	 * @param patient
	 * @return a single appointment.
	 */
	public static Appointment findAppointmentById(Long id){
		Appointment result = Appointment.find.byId(id);
		return result;
	}

	/**
	 * Find specified unconfirmed appointments.
	 * @param patient
	 * @return a single unconfirmed appointments.
	 */
	public static Appointment findUnconfirmedAppointmentById(Long id){
		Appointment result = Appointment.find.where()
											 .idEq(id)
											 .eq("Doctor_ID", null)
											 .findUnique();
		return result;
		
	}

	/**
	 * Find all future appointments.
	 * @param patient
	 * @return all appointments.
	 */
	public static List<Appointment> findAll(){
		List<Appointment> result = new ArrayList<Appointment>();
		result = Appointment.find.where()
								 .gt("StartTime_DT", DateTime.now(DateTimeZone.UTC))
								 .orderBy().asc("StartTime_DT")
								 .orderBy().asc("EndTime_DT")
								 .findList();
		return result;		
	}
	
	/**
	 * Find all future appointments associated to given patient.
	 * @param patient
	 * @return all appointments associated to given patient.
	 */
	public static List<Appointment> findAllByPatient(Patient patient){
		List<Appointment> result = new ArrayList<Appointment>();
		result = Appointment.find.where()
				 				 .gt("StartTime_DT", DateTime.now(DateTimeZone.UTC))
								 .eq("Patient_ID", patient.getId())
								 .orderBy().asc("StartTime_DT")
								 .orderBy().asc("EndTime_DT")
								 .findList();
		return result;
	}

	/**
	 * Find appointments for a given patient that conflict with a specified time.  
	 * @param patient
	 * @param startTime
	 * @param endTime
	 * @return A list of booked appointments associated to a given patient.
	 */
	public static List<Appointment> findPatientConflicts(Patient patient, DateTime startTime, DateTime endTime){
		List<Appointment> result = new ArrayList<Appointment>();
		result = Appointment.find.where()
								 .eq("Patient_ID", patient.getId())
								 .disjunction()
								 .and(Expr.le("StartTime_DT", startTime), Expr.gt("EndTime_DT", startTime))
								 .and(Expr.lt("StartTime_DT", endTime), Expr.ge("EndTime_DT", endTime))
								 .endJunction()
								 .findList();
		return result;
	}
	
	/**
	 * Find all unconfirmed, future appointments that are valid for a given doctor.
	 * @param doctor
	 * @return list of unconfirmed, future appointments.
	 */
	public static List<Appointment> findValidUnconfirmedForDoctor(Doctor doctor){
		List<Appointment> result = new ArrayList<Appointment>();

		// Nested Query
		// Step 1. Find set of conflicting appointments. Prepared as subquery.
		String sql = "select distinct t0.id "+
					 "from appointment t0, ( "+
					 "  select * from appointment where Doctor_ID = "+ doctor.getId() +
				     ") as t1 "+
					 "where (    (t0.StartTime_DT <= t1.StartTime_DT and t0.EndTime_DT > t1.StartTime_DT) "+
				     "            or (t0.StartTime_DT < t1.EndTime_DT and t0.EndTime_DT >= t1.EndTime_DT)) ";
		RawSql rawSql = RawSqlBuilder.parse(sql).create();
		Query<Appointment> subQuery =  Appointment.find.setRawSql(rawSql)
												 .where()
												 .eq("t0.Doctor_ID", null)
												 .gt("t0.StartTime_DT", DateTime.now(DateTimeZone.UTC))
												 .query();
		
		// Step 2. Find all unconfirmed appointments that are not in the set of conflicts.
		result = Appointment.find.where()
							.not(Expr.in("id", subQuery))
							.eq("t0.Doctor_ID", null)
							.orderBy().asc("t0.StartTime_DT")
							.orderBy().asc("t0.EndTime_DT")
							.findList();
		return result;
	}

}
