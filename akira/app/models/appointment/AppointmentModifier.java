package models.appointment;

import models.appointment.exceptions.AppointmentNotFoundException;
import models.appointment.exceptions.ModifiedException;
import models.role.provider.Doctor;

public class AppointmentModifier {

	public static void cancelAppointment(Long appointmentId) throws AppointmentNotFoundException, ModifiedException{
		Appointment apptToCancel = AppointmentFinder.findUnconfirmedAppointmentById(appointmentId);
		if(apptToCancel == null){
			throw new AppointmentNotFoundException("The specified appointment cannot be found.");
		}
		try {
			apptToCancel.delete();
		} catch (Exception e){
			throw new ModifiedException("The specified appointment has been modified, or no longer exists.");
		}
	}

	public static void removeDoctorFromAppointment(Long appointmentId) throws AppointmentNotFoundException, ModifiedException{
		updateDoctorForAppointment(appointmentId, null);
	}
	public static void attachDoctorToAppointment(Long appointmentId, Doctor doctor) throws AppointmentNotFoundException, ModifiedException{
		updateDoctorForAppointment(appointmentId, doctor);
	}
	private static void updateDoctorForAppointment(Long appointmentId, Doctor doctor) throws AppointmentNotFoundException, ModifiedException{
		Appointment apptToModify = AppointmentFinder.findAppointmentById(appointmentId);
		if(apptToModify == null){
			throw new AppointmentNotFoundException("The specified appointment cannot be found.");
		}
		try {
			apptToModify.setDoctor(doctor);
			apptToModify.save();
		} catch (Exception e){
			throw new ModifiedException("The specified appointment has been modified, or no longer exists.");
		}
	}
}
