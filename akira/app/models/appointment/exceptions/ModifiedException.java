package models.appointment.exceptions;

public class ModifiedException extends Exception {

	public ModifiedException(String string) {
		super(string);
	}

}
