package models.appointment.exceptions;

public class AppointmentNotFoundException extends Exception {

	public AppointmentNotFoundException(String string) {
		super(string);
	}

}
