package models.appointment.exceptions;

public class ConflictingTimeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ConflictingTimeException(String string) {
		super(string);
	}

}
