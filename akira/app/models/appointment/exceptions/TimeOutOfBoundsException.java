package models.appointment.exceptions;

public class TimeOutOfBoundsException extends Exception {

	public TimeOutOfBoundsException(String string) {
			super(string);
	}

}
