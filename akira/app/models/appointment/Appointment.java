package models.appointment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.joda.time.DateTime;

import models.baseModel.BaseModel;
import models.role.patient.Patient;
import models.role.provider.Doctor;

@Entity
@Table
public class Appointment extends BaseModel{

    @JoinColumn(name="Patient_ID", referencedColumnName="ID", nullable=false)
	@ManyToOne
    private Patient patient;
	
    @JoinColumn(name="Doctor_ID", referencedColumnName="ID")
	@ManyToOne
    private Doctor doctor = null;
    
    @Column(name="StartTime_DT", nullable=false)
    private DateTime startTime;

    @Column(name="EndTime_DT", nullable=false)
    private DateTime endTime;
    
    public Appointment(Patient patient, DateTime startTime, DateTime endTime){
    	this.patient = patient;
    	this.startTime = startTime;
    	this.endTime = endTime;
    }
    
    //Getters & Setters
    public DateTime getStartTime(){
    	return startTime;
    }
    public DateTime getEndTime(){
    	return endTime;
    }
    public Doctor getDoctor(){
    	return doctor;
    }
    public Patient getPatient(){
    	return patient;
    }
    
    public void setStartTime(DateTime startTime){
    	this.startTime = startTime;
    }
    public void setEndTime(DateTime endTime){
    	this.endTime = endTime;
    }
    public void setDoctor(Doctor doctor){
    	this.doctor = doctor;
    }
    
    public static Finder<Long, Appointment> find = new Finder<>(Appointment.class);
}
