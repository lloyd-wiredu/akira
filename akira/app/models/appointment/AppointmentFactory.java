package models.appointment;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import models.appointment.exceptions.ConflictingTimeException;
import models.appointment.exceptions.TimeOutOfBoundsException;
import models.role.patient.Patient;
import play.Logger;

public class AppointmentFactory {
	
	/**
	 * Create new appointment with specified parameters and save
	 * @param start		the start time of appointment as long value from client side.
	 * @param max		the maximum allowable end time of an appointment.
	 * @param days		the number of days in the future from day of request.
	 * @param duration	the duration of the appointment.
	 * @param patient	the patient object representing the requestor
	 * @return			the saved appointment object
	 * @throws ConflictingTimeException
	 * @throws TimeOutOfBoundsException
	 */
	public static Appointment newAppointment(long start, long max, int days, int duration, Patient patient) throws ConflictingTimeException, TimeOutOfBoundsException{
		// Create datetime objects for long values in UTC timezone.
		DateTime maxTime = new DateTime(max, DateTimeZone.UTC).plusDays(days);
		DateTime startTime = new DateTime(start, DateTimeZone.UTC).plusDays(days);
		DateTime endTime = new DateTime(start, DateTimeZone.UTC).plusDays(days).plusMinutes(duration);
		
		// Check validity of submitted times.
		if(endTime.isAfter(maxTime)){
			throw new TimeOutOfBoundsException("You have exceeded the bookable time range.");
		}		
		if(!AppointmentFinder.findPatientConflicts(patient, startTime, endTime).isEmpty()){
			throw new ConflictingTimeException("The selected time conflicts with an existing appointment.");
		}
		
		// Create the appointment entry.
		Appointment newAppointment = new Appointment(patient, startTime, endTime);
		try {
			newAppointment.save();
		} catch (Exception e) {
			Logger.error("Failed to save appointment.");
		}
		return newAppointment;
	}
	
}
