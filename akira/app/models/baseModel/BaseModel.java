package models.baseModel;

import org.joda.time.*;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.Model;

@MappedSuperclass
public class BaseModel extends Model {
	private static final long serialVersionUID = 1L;

    @Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(length=20, nullable=false)
	private Long id;
    
    @CreatedTimestamp
    @Column(columnDefinition="DATETIME DEFAULT CURRENT_TIMESTAMP")
    private DateTime created = DateTime.now();
 
    @Version
    @Column(columnDefinition="DATETIME DEFAULT CURRENT_TIMESTAMP")
    private DateTime updated = DateTime.now();
    
    public BaseModel() {}

	public Long getId() {
		return id;
	}

	public DateTime getCreated() {
		return created;
	}

	public DateTime getUpdated() {
		return updated;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public void setUpdated(DateTime updated) {
		this.updated = updated;
	}
}